Limitations
===========

This is a simple developer tool. It works but is not extensively tested for
*boundary conditions* and as such has limited error-handling so it might emit
some rather unpleasant errors if something goes wrong. Having said that,
it works but it's a *work-in-progress* and will be refined, as needed, as
time goes by.

Known Limitations
-----------------

1.  List re-ordering is not implemented. This program sets the lists in
    their ``position`` order but cannot re-order them if you change the
    position field in the definition file.
2.  Issue boards are not created automatically here. You will need to navigate
    to your new project and navigate to ``Issues > Boards`` so that GitLab
    can create an empty board for you that this tool can then initialise. if
    board does nto exist ``lablabel`` wil tell you.
