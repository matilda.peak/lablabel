Running lablabel
================

.. highlight:: none

You can get help with the utility using ``--help``::

    lablabel --help

To setup the ``proj`` repository for the user ``blob``, with the personal
access code ``1234``, run the following in a directory that contains your
``GITLAB-LABELS.md`` file::

    lablabel blob proj 1234 set

And update the project with::

    lablabel blob proj 1234 update

It is safe to run a ``set`` or ``update`` on a project that already contains
the defined labels.

A simple test mode can be used to run the utility on a file without writing
the label definitions to Git::

    lablabel blob proj 1234 --test

A good working practice is to add a ``GITLAB-LABELS.md`` file to the root of
your project and run ``lablabel`` from there. You then have a permanent
record of the label definitions and, if you need to add or adjust your labels
in the future, edit the file and just re-run ``lablabel``
(see :doc:`limitations` when doing this).
