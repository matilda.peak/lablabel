The Label Definition File
=========================

.. highlight:: none

``lablabel`` creates labels and lists based on the content of a `markdown`_
file whose name is ``GITLAB-LABELS.md`` that is located in the
current working directory or execution directory.

It's a standard markdown file but it is expected to contain the following
sub-heading, which starts a block that defines the project labels::

    ## Common labels

Labels are interpreted between this sub-heading ans the next (or the end of
he file). Each label is defined in a sub-subheading (``###``) and is a list
of bullet-points defining the ``Colour``, ``Priority`` and ``Description``
of each label.

Here's an example of a block that contains definitions for the labels
``Blocker`` and ``Documentation``::

    ## Common labels

    ### Blocker

    -   Colour: #FF0000
    -   Priority: 0
    -   Description: Urgent faults presenting significant problems that need
                     to be addressed at the highest priority. They either
                     block features or break production functionality.

    ### Documentation

    -   Colour: #428BCA
    -   Priority: 4
    -   Description: An issue relating to documentation, either a fault
                     with an existing document or the need for a new
                     document

    ## Some other stuff

*   The ``colour`` should be a 6-digit hex value preceded with ``#``
*   The ``Priority`` should be a number
*   The ``Description`` is text and it may span more than one line (as shown)

List Labels
-----------

Black labels (those with a colour value of ``#000000``) are used to create
lists and are expected to define a ``position`` rather than a ``priority``.

.. _markdown: https://daringfireball.net/projects/markdown/syntax
