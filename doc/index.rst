lablabel
========

``lablabel`` is a label utility for GitLab projects. Given a simple markdown
text file of label definitions this utility sets up (or updates) a GitLab
repository's labels and lists. The text file accommodates the definition of
labels, colours, priorities and descriptions. It interprets
any black labels as lists, creating a set of lists for your boards.

To use the utility you will need: -

*   A text file of label definitions
*   A GitLab project
*   A GitLab *Personal Access Code* (or `PAT`_)

.. _pat: https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :titlesonly:

   installation
   definition-file
   running
   limitations
