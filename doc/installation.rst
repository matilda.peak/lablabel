Installation
============

``lablabel`` can be found on `GitLab`_ and is published on `PyPI`_ and can be
installed from there::

    pip install matildapeak-lablabel

.. _PyPI: https://pypi.org/project/matildapeak-lablabel
.. _GitLab: https://gitlab.com/matilda.peak/lablabel
