# Issue labels
Issue labels for our projects so that issues will be rapidly identified
through a consistent use of label naming and colouring for a GitLab
issue board.

>   This file is inspected by the 'lablabel` module
    to automatically extract labels and lists. Feel free
    to use this fie asa template example. If copied to 'GITLAB-LABELS.md'
    it wil be excluded from submission to Git.

## Common labels
We describe the expected labels for each project with an accompanying
description of the topics it should be used for and how/when it should be
used. We also recommend a colour coding for systems that support it.

>   Note: The colour values are in `sRGB` format.

Now ... an uninterrupted set of label (and list) definitions...

### Blocker

-   Colour: #FF0000
-   Priority: 0
-   Description: Urgent faults presenting significant problems that need
                 to be addressed at the highest priority. They either
                 block features or break production functionality.

### Bug

-   Colour: #CC0033
-   Priority: 1
-   Description: A fault that can lead to incorrect program behaviour

### Flaw

-   Colour: #FF4476
-   Priority: 2
-   Description: A fault that can lead to inefficient operation of the program

### Stability

-   Colour: #D10069
-   Priority: 2
-   Description: An issue relating to software stability. This is
                 either something that can be reproduced or may be
                 intermittent but basically leads to a failure
                 of some kind. Consider a `Blocker` if the issue
                 affects service

### Usability

-   Colour: #A8D695
-   Priority: 3
-   Description: An issue relating to the user interface or API

### Documentation

-   Colour: #428BCA
-   Priority: 4
-   Description: An issue relating to documentation, either a fault
                 with an existing document or the need for a new
                 document

### Enhancement

-   Colour: #5CB85C
-   Priority: 5
-   Description: A request for a software enhancement to an existing
                 software feature

### Feature Request

-   Colour: #004E00
-   Priority: 6
-   Description: A suggestion for a new product feature

### Discussion

-   Colour: #F0AD4E
-   Priority: 7
-   Description: An issue for discussion, which may turn into a `Bug`
                 `Feature Request` or another issue type

### Blocked

-   Colour: #000000
-   Position: 1
-   Description: Stuff that is blocked for some reason.

### To Do

-   Colour: #000000
-   Position: 2
-   Description: Stuff out of the `Backlog` and is a candidate for
                 current development.

### In Progress

-   Colour: #000000
-   Position: 3
-   Description: Stuff that is actively in progress.
