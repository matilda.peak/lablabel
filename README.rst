A label utility for GitLab
==========================

.. image:: https://gitlab.com/matilda.peak/lablabel/badges/master/pipeline.svg
   :target: https://gitlab.com/matilda.peak/lablabel
   :alt: Pipeline Status (lablabel)

.. image:: https://badge.fury.io/py/matildapeak-lablabel.svg
   :target: https://badge.fury.io/py/matildapeak-lablabel
   :alt: PyPI package (latest)

.. image:: https://readthedocs.org/projects/lablabel/badge/?version=latest
   :target: https://lablabel.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status (lablabel)

``lablabel`` is labelling utility for GitLab projects. Given a text file of
label definitions this utility attempts to create those labels in the given
GitLab project.

The text file defines the name, colour, priority and description of each
of your project labels. Now, armed with a suitable file you can setup
all your projects quickly using a consistent label style.

Installation
------------

LabLabel is published on `PyPI`_ and can be installed from
there::

    pip install matildapeak-lablabel

.. _PyPI: https://pypi.org/project/matildapeak-lablabel

Documentation
-------------

LabLabel documentation is available on `Read The Docs`_.

.. _Read The Docs: https://lablabel.readthedocs.io/en/latest/

Get in touch
------------

- Report bugs, suggest features or view the source code `on GitLab`_.

.. _on GitLab: https://gitlab.com/matilda.peak/lablabel.git
